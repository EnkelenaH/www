<!--#set var="title" value="DebConf Code of Conduct"--><!--#include virtual="header.shtml" -->
<!--
vim:ts=2:sw=2:et:ai:sts=2:tw=78
-->
      <div id="TEXT">
        <ul class="Path">
          <li><a href="/">Home</a></li>
          <li>DebConf Code of Conduct</li>
        </ul>
        <h1>DebConf Code of Conduct</h1>

        <p>
          DebConf, as part of the greater Debian Community, assumes good faith
          on all those who wish to improve Debian.
          However, other experiences at other conferences have shown us the
          need to adopt a Code of Conduct in which we state our expectations
          of all attendees and organizers during the Debian Conference.
        </p>

        <p>
          This code of conduct applies to all attendees at DebConf, in
          addition to the
          <a href="https://www.debian.org/code_of_conduct">Debian code of
            conduct</a> that applies to the Debian community as a whole.
        </p>

        <h2>Debian Diversity Statement</h2>

        <p>
          The Debian Project welcomes and encourages participation by
          everyone.
        </p>
        <p>
          No matter how you identify yourself or how others perceive you: we
          welcome you.
          We welcome contributions from everyone as long as they interact
          constructively with our community.
        </p>

        <p>
          While much of the work for our project is technical in nature, we
          value and encourage contributions from those with expertise in other
          areas, and welcome them into our community.
        </p>

        <h2>Be excellent to each other</h2>

        <p>
          DebConf is committed to providing a safe environment for all
          participants.
          All attendees are expected to treat all people and facilities with
          respect and help create a welcoming environment.
          If you notice behavior that fails to meet this standard, please
          speak up and help to keep DebConf as respectful as we expect it to
          be.
        </p>

        <p>
          DebConf is committed to the ideals expressed in our Diversity
          Statement (above) and the recently adopted Debian Code of Conduct.
          We ask all our members, speakers, volunteers, attendees and guests
          to adopt these principles.
          We are a diverse community.
          Sometimes this means we need to work harder to ensure we're creating
          an environment of trust and respect where all who come to
          participate feel comfortable and included.
        </p>

        <p>
          We value your participation and appreciate your help in realizing
          this goal.
        </p>

        <h2>Be respectful</h2>

        <p>
          Respect yourself, and respect others.
          Be courteous to those around you.
          If someone indicates they don't wish to be photographed, respect
          that wish.
          If someone indicates they would like to be left alone, let them be.
          Our event venues and online spaces may be shared with members of the
          public; please be considerate to all patrons of these locations,
          even if they are not involved in the conference.
        </p>

        <h2>Be inclusive</h2>

        <p>
          By default, all presentation material should be suitable for people
          aged 12 and above.
        </p>

        <p>
          If you could reasonably assume that some people may be offended by
          your talk, please state so explicitly in the submission notes.
          This will be taken into account by the Content Team.
          In case you are unsure if this applies to you, please contact the
          Content Team at
          <a href="mailto:content@debconf.org">content@debconf.org</a>.
          Please note that you are solely responsible if anything is deemed
          inappropriate and you did not contact the Content Team beforehand.
        </p>

        <h2>Be aware.</h2>

        <p>
          We ask everyone to be aware that we will not tolerate intimidation,
          harassment, or any abusive, discriminatory or derogatory behavior by
          anyone at any Debian event or in related online media.
        </p>

        <p>
          Complaints can be made to the organizers by contacting the
          registration desk or emailing <a
            href="mailto:antiharassment@debian.org">antiharassment@debian.org</a>.
          All complaints made to event organizers will remain confidential and
          be taken seriously.
          The complaint will be treated appropriately and with discretion.
          Should event organizers or moderators consider it appropriate,
          measures they may take can include:
        </p>

        <ul>
          <li>the individuals may be told to apologize</li>
          <li>
            the individuals may be told to stop/modify their behavior
            appropriately
          </li>
          <li>
            the individuals may be warned that enforcement action may be taken
            if the behavior continues
          </li>
          <li>
            the individuals may be asked to immediately leave the venue and/or
            will be prohibited from continuing to attend any part of the event
          </li>
          <li>
            the incident may be reported to the appropriate authorities
          </li>
        </ul>

        <h2>What does that mean for me?</h2>

        <p>
          All participants, including event attendees and speakers must not
          engage in any intimidation, harassment, or abusive or discriminatory
          behavior.
        </p>

        <p>
          The following is a list of examples of behavior that is deemed
          highly inappropriate and will not be tolerated at DebConf:
        </p>

        <ul>
          <li>
            offensive verbal or written remarks related to gender, sexual
            orientation, disability, physical appearance, body size, race, or
            religion;
          </li>
          <li>
            sexual or violent images in public spaces (including presentation
            slides);
          </li>
          <li>deliberate intimidation;</li>
          <li>stalking or following;</li>
          <li>unwanted photography or recording;</li>
          <li>sustained disruption of talks or other events;</li>
          <li>unwelcome physical contact or other forms of assault;</li>
          <li>unwelcome sexual attention;</li>
          <li>sexist, racist, or other exclusionary jokes;</li>
          <li>
            unwarranted exclusion from conference or related events based on
            age, gender, sexual orientation, disability, physical appearance,
            body size, race, religion;
          </li>
        </ul>

        <p>
          We want everyone to have a good time at our events.
        </p>

        <h2>Questions?</h2>

        <p>
          If you’re not sure about anything in this conference Code of
          Conduct, please contact the DebConf organizers at <a
            href="debconf-team@lists.debian.org">debconf-team@lists.debian.org</a>.
        </p>

        <p>
          If you wish to report a violation of this Code of Conduct, please
          contact <a
            href="antiharassment@debian.org">antiharassment@debian.org</a>.
        </p>

        <h2>Our Promise to You.</h2>

        <ul>
          <li>
             We will read every complaint and have several people on that alias that
             can help investigate and resolve the complaint.
          </li>
          <li>
            We will reply, in writing, as soon as possible to acknowledge the concern
            and assure that the matter is being investigated.
          </li>
          <li>
            Depending on the situation, we will talk to the reporter, the reported, or
            both to determine what mediation and/or action is necessary.
          </li>
          <li>
            Depending on the outcome of the investigation and mediation, we reserve
            the right to expel people not in compliance with our Code of Conduct from
            the venue. Debian, the DebConf Organizing Committee and the venue in which
            DebConf is being held will not be held responsible for further costs
            incurred by the dismissal from the conference.
          </li>
        </ul>

      </div>
<!--#include virtual="footer.shtml" -->
